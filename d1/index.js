// console.log("Hello World!");


//Repetition Control Structure (Loops)
	//Loops are one of the most important feature that programming must have
	//It lets us execute code repeatedly in a pre-set number or maybe forever

	/*
		Mini Activity
		1. Create a function named greeting() and display "Hi, Batch 211!" using console.log inside the function
		2. Invoke the greeting () function 10 times
		3. Take a screenshot of your work's console and send it to the batch hangouts
	*/

	//solution

	function greeting(){
		console.log("Hi, Batch 211!");
	};
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();

	//or we can just use loops

	let countNum = 10;

	while(countNum !==0){
		console.log("This is printed inside the loop: " + countNum);
		greeting()
		countNum--;
	}

	//While Loop
	/*
		A while loop takes in an expression/condition
		If the conditionn evalautes to true, the statements inside the code block will be executed
	*/
	/*
		Syntax
			while(expression/condition){
				statement/code block
				final expression ++/-- (iteration)
			}

			-expression/condition - this are the unit of code that is being evaluated in our loop
			Loop will run while the condition/exprression is true

			-statement/code block - code/instructions that will be executed several times

			-finalExpression - indicates how to advance the loop

	*/

	// let count = 5;
	// //While the value of count is not equal to 0
	// while(count !==0){
	// 	//the current value of count is printed out
	// 	console.log("While: " + count);
	// 	//decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
	// 	count --;
	// }

	// let num = 20;

	// while(num !== 0){
	// 	console.log("While: num " + num)
	// 	num --;
	// }

	// let digit = 5;
	// while(digit !==20){
	// 	console.log("While: digit " + digit)
	// 	digit ++;
	// }

	// let num1 = 1;
	// while(num1 === 2){
	// 	console.log("While: num1 " + num1);
	// 	num--
	// }


	//Do While Loop

	/*
		- A do-while loop works a lot like the while loop
		But unlike while loops, do-while loops guarantee that the code will be executed at lease once
	*/

	/*
		Syntax
		do {
			statement/code block
			finalExpression ++/--
		}while (expression/condition)

	*/

	/*
	How the Do While Loop Works:
	1. The sttaments in the "do" block executes once
	2. The message "Do While: " + number will be printed out in the console
	3. After executing once, the while sttamenet will evaluate whether to run the next iteration of the loop based on the given expression/condition 
	4. If the expression/condition is not true, another iteration of the loop will be executed and will be repeated until the condition is met
	5. If the condition is true, the loop will stop


	*/



	let number = Number(prompt("Give me a number"));
	do {
		console.log("Do while: " + number)
		number += 1;
	}while(number<10)


	//For Loop

	//A For Loop is more flexible than while and do-while loops
	/*
		It consists of three parts:
		1. Initialization value that will track the progression of the loop
		2. Expression/Condition that will be evaluated which will determine if the loop will run one more time
		3. finalExpression indicates how to advance the loop
	*/
	/*
		Syntax

		for (initialization; expression/condition; finalExpression){
			statement
		}
	*/

	for (let count = 0; count <=20; count++){
		console.log("For Loop: " + count);
	}

	/*
		Mini Activity
		Refactor the code above that the loop will only print the even numbers
		Take a screenshot of your work's console and send it to our batch chat
	*/

	//solution

	for (let count = 0; count <=20; count++){
		// console.log("For Loop: " + count);
		if(count % 2 == 0 && count != 0){
			console.log("Even: " + count);
		}
	}


	let myString = "Camille Doroteo";
	//characters in strings may be counted using the .length property
	//strings are special compared to other data types
	console.log(myString.length);

	//Accesssing elements of a string
	//individual characters of a string may be accessed using its index number
	//the first character in a string corresponds to the number 0, the next is 1...

	console.log(myString[2]);
	console.log(myString[0]);
	console.log(myString[8]);
	console.log(myString[14]);

	for(let x = 0; x < myString.length; x++){
		console.log(myString[x])
	};

	/*
		Mini Activity
		create  a variable named myFullName
		create a for loop
	*/

	let myName = "NehEmIAh"; //Nhmh
	let myNewName = "";

	for (let i=0; i<myName.length; i++){

		// console.log(myName[i].toLowerCase());
		 // If the character of your name is a vowel letter, instead of displaying the character, display number "3"
    	// The ".toLowerCase()" function/method will change the current letter being evaluated in a loop to a lowercase letter ensuring that the letters provided in the expressions below will match 

		if(
			myName[i].toLowerCase() == "a" ||
			myName[i].toLowerCase() == "i" ||
			myName[i].toLowerCase() == "u" ||
			myName[i].toLowerCase() == "e" ||
			myName[i].toLowerCase() == "o"
		){
			// If the letter in the name is a vowel, it will print the number 3
			console.log(3);
		}else{
			// Print in the console all non-vowel characters in the name
			console.log(myName[i]);
			myNewName += myName[i];
		}
	}
	console.log(myNewName);
	//concatenate the characers based on the given condition

	//Continue and Break Stataments
	/*
		The continue statement allows the code to go tot he next iteration of the loop without finishing the execution of all statements in a code block

		The break statement is used to terminate the current loop once a match has been found

	*/

	for(let count = 0; count <=20; count++){

		if(count % 2 === 0){
			console.log("Even Number")
			//tells the console to continue to the next iteration of the loop
			continue;
			//this ignores all statements located after the continue statement
			// console.log("Hello")
		}
		console.log('Continue and Break: ' + count);

		if(count>10){
			//tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
			//number values after 10 will no longer be printed
			break;
		}

	}

	let name = "alexandro";

	for (let i = 0; i <name.length; i++){
		//the current letter is printed out based on its index 
		console.log(name[i]);

		 // If the vowel is equal to a, continue to the next iteration of the loop
		if (name[i].toLowerCase() === "a"){
			console.log("Continue to the next iteration");
			continue;
		}

		 // If the current letter is equal to d, stop the loop
		if(name[i] == "d"){
			break;
		}
	}